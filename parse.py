from __future__ import print_function
from Bio.Blast import NCBIXML
from Bio import SeqIO

def main(fname, dbname, outdir, cutoff=1.0e-150):
    db = SeqIO.index_db("./faidx.db", dbname, "fasta")
    ifh = open(fname)
    xml = NCBIXML.parse(ifh)
    for rec in xml:
        qry = rec.query
        print("Processing contig: {}".format(qry))
        qry_dict = {}
        qry_ofh = open("{}/{}.goodseqs.fa".format(outdir, qry), "w")
        for aln in rec.alignments:
            expts = []
            hit = aln.hit_def.split()[0]
            for hsp in aln.hsps:
                expts.append(hsp.expect)
            qry_dict[hit] = min(expts)
        good_seqs = 0
        for k, v in qry_dict.items():
            if v <= cutoff:
                print("Writing seq '{}', {} <= {}".format(k, v, cutoff))
                SeqIO.write(db[k], qry_ofh, "fasta")
                good_seqs +=1
        print("Wrote {} good seqs for {}!\n".format(good_seqs, qry))


#if __name__ == "__main__":
#    import sys
#    main(sys.argv[1])
#
